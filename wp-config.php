<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the installation.
 * You don't have to use the web site, you can copy this file to "wp-config.php"
 * and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://wordpress.org/support/article/editing-wp-config-php/
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'wordpress' );

/** MySQL database username */
define( 'DB_USER', 'wordpress' );

/** MySQL database password */
define( 'DB_PASSWORD', '6b0741ea96fc97631fc50b3fe72a0a09cab4016e045a9cdd' );

/** MySQL hostname */
define( 'DB_HOST', 'localhost' );

/** Database charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8' );

/** The database collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**#@+
 * Authentication unique keys and salts.
 *
 * Change these to different unique phrases! You can generate these using
 * the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}.
 *
 * You can change these at any point in time to invalidate all existing cookies.
 * This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         ';$Ll.b5othgM/Kn#:c:m|NP;S?g$d@Q%k5tD PL@BdBN(|cBE~s_SJUXDb_s`DHQ' );
define( 'SECURE_AUTH_KEY',  '{_d}R(>eDF$g/cExPH&{}wzD)7P?v8VPR.qEd[~BFa[/!C^p =?@q 1*_$,*e~(i' );
define( 'LOGGED_IN_KEY',    'muPKEdYWu0HLiW_+a/:q9heEWlmw:C0b>^BwcI@u8aDugw(%X%$]7{-A>1h!<K^k' );
define( 'NONCE_KEY',        'ph2?sn_F%RT9UKbA|PR.Gi?bu1B>i>;>UR4k;!iB<s^sz1#Q<gys>1Ym?%t)oTAv' );
define( 'AUTH_SALT',        '4dI`}T.qU<z:/_^J=L=K?)~/DlI|XGH~iUJ6XP+K4}<sFE|bNS894p)C)3z|uB7`' );
define( 'SECURE_AUTH_SALT', '8+r6GHU[ m4!,i[EQh:^=5>p:zfy0G/Ya$LEc*<}NEr/)Ym+0E[9UT|`(jzGt,|+' );
define( 'LOGGED_IN_SALT',   'iR{}~SnQJ:;OUBnvuVsUDdk[>d3$tUm^|13m-&C5SD(rNq2a/uL)o60pCE[bzdf6' );
define( 'NONCE_SALT',       'NG:u0A4JGax_^d]$GLy~05EDOl[/]r=aHAcL-`vc*,YlFKl>bgu:AS{[I]6Yo%/T' );

/**#@-*/

/**
 * WordPress database table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the documentation.
 *
 * @link https://wordpress.org/support/article/debugging-in-wordpress/
 */
define( 'WP_DEBUG', false );

/* Add any custom values between this line and the "stop editing" line. */



/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', __DIR__ . '/' );
}

/** Sets up WordPress vars and included files. */
require_once ABSPATH . 'wp-settings.php';
